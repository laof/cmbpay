package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;
import lombok.NoArgsConstructor;

/**
 * {
 *         "dateTime":"20161216140241",
 *         "date":"20161216",
 *         "amount":"0.01",
 *         "bankDate":"20161216",
 *         "orderNo":"9999153784",
 *         "discountAmount":"0.00",
 *         "noticeType":"BKPAYRTN",
 *         "httpMethod":"POST",
 *         "cardType":"02",
 *         "noticeSerialNo":"0755000054201612169999153784",
 *         "merchantPara":"aaa",
 *         "discountFlag":"N",
 *         "bankSerialNo":"16321686300000000010",
 *         "noticeUrl":"http://www.merchant.com/path/payNotice.do",
 *         "branchNo":"0755",
 *         "merchantNo":"000054"
 *     }
 */
@Getter
@Setter
public class NoticePayReqDTO {

    private String dateTime;
    private String date;
    private String amount;
    private String bankDate;
    private String orderNo;
    private String discountAmount;
    private String noticeType;
    private String httpMethod;
    private String cardType;
    private String noticeSerialNo;
    private String merchantPara;
    private String discountFlag;
    private String bankSerialNo;
    private String noticeUrl;
    private String branchNo;
    private String merchantNo;

    /**
     *
     * 请求时间	dateTime	String(14)	M	商户发起支付请求的时间，精确到秒
     * 格式：yyyyMMddHHmmss	20161216140241
     *
     * 回调HTTP地址	noticeUrl	String(256)	M	回调HTTP地址,支付请求时填写的支付结果通知地址	http://www.merchant.com/path/payNotice.do
     *
     * 回调HTTP方法	httpMethod	String(10)	M	固定为“POST”	POST
     *
     * 商户分行号	branchNo	String(4)	M	商户分行号，4位数字	0755
     *
     * 商户号	merchantNo	String(6)	M	商户号，6位数字	000054
     *
     * 通知类型	noticeType	String(8)	M	本接口固定为：“BKPAYRTN”	BKPAYRTN
     *
     * 银行通知序号	noticeSerialNo	String(40)	M	银行通知序号,订单日期+订单号	20180506201612169999153784
     *
     * 订单日期	date	String(8)	M	商户订单日期
     * 格式：yyyyMMdd	20161216
     *
     * 订单号	orderNo	String(32)	M	商户订单号	9999153784
     *
     * 金额	amount	String(14)	M	订单金额，格式：XXXX.XX	0.01
     *
     * 银行受理日期	bankDate	String(8)	M	银行受理日期	20161216
     *
     * 银行订单流水号	bankSerialNo	String(20)	M	银行订单流水号	16321686300000000010
     *
     * 优惠标志	discountFlag	String(1)	M	优惠标志,Y:有优惠 N：无优惠	Y
     *
     * 优惠金额	discountAmount	String(14)	M	单位为元，精确到小数点后两位。格式为：xxxx.xx元	0.00
     *
     * 商户附加参数	merchantPara	String(256)	O	原样返回商户在一网通支付请求报文中传送的成功支付结果通知附加参数	aaa
     *
     * 支付卡类型	cardType	String(2)	O	卡类型,02：本行借记卡；
     * 03：本行贷记卡；
     * 08：他行借记卡；
     * 09：他行贷记卡	02
     *
     */
}
