package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;

@Getter  @Setter
public class CmbGetDownloadURLReqDTO {


    /**
     * 商户发起该请求的当前时间，精确到秒
     * 格式：yyyyMMddHHmmss
     */
    private String dateTime ;

    /**
     * 商户分行号，4位数字
     */
    private String branchNo ;

    /**
     * 商户号，6位数字
     */
    private String merchantNo ;

    /**
     * 查询日期,格式：yyyyMMdd
     */
    private String date ;

    /**
     * 交易类型，固定为“4001”
     */
    private String transactType = "4001";

    /**
     * 文件类型，固定为“YBL”
     */
    private String fileType = "YBL";

    /**
     * 交易流水，合作方内部唯一流水
     */
    private String messageKey ;



}
