package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;
import lombok.NoArgsConstructor;

/**
 *
 * 成功签约结果通知API
 * /**
 *      * dateTime : 20160622182921
 *      * noticeType : BKQY
 *      * noticeSerialNo : 201606238888888
 *      * branchNo : 0755
 *      * merchantNo : 002346
 *      * noticeUrl : https://...
 *      * httpMethod : POST
 *      * agrNo : 0001283
 *      * noticePara :
 *      * rspCode : SUC0000
 *      * rspMsg :
 *      * userID : 12345
 *      * userPidType : 1
 *      * userPidHash : 501010131010181016101010101012
 *      * noPwdPay : N
 *      */
@Getter  @Setter
public class NoticeQianYueReqDTO {

    private String dateTime;
    private String noticeType;
    private String noticeSerialNo;
    private String branchNo;
    private String merchantNo;
    private String noticeUrl;
    private String httpMethod;
    private String agrNo;
    private String noticePara;
    private String rspCode;
    private String rspMsg;
    private String userID;
    private String userPidType;
    private String userPidHash;
    private String noPwdPay;
}
