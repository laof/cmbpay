package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;

/**
 {
 "version":"1.0",
 "charset":"UTF-8",
 "sign":"见签名处理章节",
 "signType":"SHA-256",
 "rspData":{
     "rspCode":"SUC0000",
     "rspMsg":"查询成功。",
     "fbPubKey":"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC8IdQSg5ryvST/IC5aAjNY5sFw+qhVkyFKGqQFQCZfXa9YqsbhZFhSBZHLUw+TIJO8axH7Sa+OM4AEmoddvaPrnXRB7XJmUQMXj77AnGStzzJfQddMGMJiKa4KT6wKsmQxjC3NdKrYLVQntb9qD+M3AqIK5WBvbh/ix5GUTmUQ+QIDAQAB",
     "dateTime":" 20160623101430"
 }
 }
 *
 *      */
@Getter  @Setter
public class CmbPubKeyRespDTO {

    /**
     * 处理结果,SUC0000：请求处理成功
     * 其他：请求处理失败
     */
    private String rspCode ;

    /**
     * 详细信息,失败时返回具体失败原因
     */
    private String rspMsg  ;

    /**
     * 用Base64编码的招行公钥
     */
    private String fbPubKey ;

    /**
     * 响应时间,银行返回该数据的时间
     * 格式：yyyyMMddHHmmss
     */
    private String dateTime ;


}
