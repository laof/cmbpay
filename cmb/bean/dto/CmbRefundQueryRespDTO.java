package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;
import lombok.NoArgsConstructor;

/***
 */
@NoArgsConstructor
@Getter  @Setter
public class CmbRefundQueryRespDTO {

    /**
     *
     */
    private String rspCode;
    private String rspMsg;
    private String dateTime;
    private String hasNext;
    private String nextKeyValue;
    private String dataCount;

    /**
     *
     * 记录数据,每笔记录一行，行之间以\r\n分隔。 第一行为表头; 从第二行起为数据记录;
     * 行内的参数以逗号和`符号分隔(`为标准键盘1 左边键的字符)，字段顺序与表头一致。 数据记录定义如下：
     *
     * branchNo	String(4)	M	商户分行号，4位数字	0755
     * merchantNo	String(6)	M	商户号，6位数字	000054
     * date	String(8)	M	商户订单日期，格式：yyyyMMdd	20160624
     * orderNo	String(32)	M	商户订单号	9999000001
     * bankSerialNo	String(20)	M	银行的订单流水号	16250327200000000020
     * orderRefNo	String(32)	O	订单参考号
     * refundSerialNo	String(20)	M	银行退款流水号	16280535600000000010
     * orderStatus	String(3)	M	210：已直接退款
     * 219：直接退款已受理
     * 240：已授权退款
     * 249：授权退款已受理
     * 999：退款失败	210
     * currency	String(2)	M	币种,固定为：“10”	10
     * amount	String	M	退款金额,格式：xxxx.xx	0.01
     * fee	String	M	费用金额,格式：xxxx.xx	0.01
     * refundDesc	String	O	退款说明
     * bankDate	String(8)	M	银行受理日期,格式：yyyyMMdd	20160624
     * bankTime	String(6)	M	银行受理时间,格式：HHmmss	121201
     * settleAmount	String	M	实际退款金额,格式：xxxx.xx	0.01
     * discountAmount	String	M	退回优惠金额,格式：xxxx.xx	0.01
     */
    private String dataList;
}
