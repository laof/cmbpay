package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;

/**
 *     /**
 *      {
 *      "version":"1.0",
 *      "charset":"UTF-8",
 *      "sign":"见签名处理章节",
 *      "signType":"SHA-256",
 *      "reqData":{
 *      "dateTime":"20160623101430",
 *      "txCode":"FBPK",
 *      "branchNo":"0755",
 *      "merchantNo":"002346"
 *      }
 *      }
 *
 *      */
@Getter  @Setter
public class CmbPubKeyReqDTO {


    /**
     * 商户发起该请求的当前时间，精确到秒
     * 格式：yyyyMMddHHmmss
     */
    private String dateTime ;

    /**
     * 交易码,固定为“FBPK”
     */
    private String txCode = "FBPK" ;

    /**
     * 商户分行号，4位数字
     */
    private String branchNo ;

    /**
     * 商户号，6位数字
     */
    private String merchantNo ;


}
