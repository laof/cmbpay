package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;

@Getter  @Setter
public class CmbBusinessReqDTO {


    /**
     * 商户发起该请求的当前时间，精确到秒
     * 格式：yyyyMMddHHmmss
     */
    private String dateTime ;

    /**
     * 交易码,固定为“FBPK”
     */
    private String txCode ;

    /**
     * 商户分行号，4位数字
     */
    private String branchNo ;

    /**
     * 商户号，6位数字
     */
    private String merchantNo ;

    /**
     * 商户做此查询请求的流水号
     */
    private String merchantSerialNo ;

    /**
     * 客户签约的协议号
     */
    private String agrNo ;


}
