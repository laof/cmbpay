package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;


@Getter  @Setter
public class CmbQuerySettledOrderByBankDateReqDTO {


    /**
     * 商户发起该请求的当前时间，精确到秒
     * 格式：yyyyMMddHHmmss
     */
    private String dateTime ;

    /**
     * 商户分行号，4位数字
     */
    private String branchNo ;

    /**
     * 商户号，6位数字
     */
    private String merchantNo ;

    /**
     * 开始日期,格式：yyyyMMdd
     */
    private String beginDate ;

    /**
     * 结束日期,格式：yyyyMMdd
     */
    private String endDate ;

    /**
     * 操作员号,商户结账系统的操作员号
     */
    private String operatorNo ;
    /**
     * 续传键值,长度只能为0或40；首次查询填“空”；后续查询，按应答报文中返回的nextKeyValue值原样传入。
     */
    private String nextKeyValue ;


}
