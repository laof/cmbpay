package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;
import lombok.NoArgsConstructor;

/***
 */
@NoArgsConstructor
@Getter  @Setter
public class CmbQueryAccountListReqDTO {


    /**
     * 请求时间,商户发起该请求的当前时间，精确到秒。 格式：yyyyMMddHHmmss
     */
    private String dateTime;
    /**
     * 商户分行号，4位数字
     */
    private String branchNo;
    /**
     * 商户号，6位数字
     */
    private String merchantNo;
    /**
     * 查询日期,格式：yyyyMMdd
     */
    private String date;
    /**
     * 商户结账系统的操作员号
     */
    private String operatorNo;
    /**
     * 续传键值,首次查询填“空”; 后续查询，按应答报文中返回的nextKeyValue值原样传入。
     */
    private String nextKeyValue;
}
