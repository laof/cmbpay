package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@Getter  @Setter
public class CmbOrderQueryRespDTO {


    /**
     * rspCode : SUC0000
     * rspMsg :
     * dateTime : 20160825140752
     * branchNo : 0755
     * merchantNo : 000054
     * date : 20160503
     * orderNo : 9999030401
     * bankSerialNo : 16250327200000000020
     * currency : 10
     * orderAmount : 0.01
     * fee : 0.00
     * bankDate : 20160503
     * bankTime : 205620
     * settleAmount : 0.01
     * discountAmount : 0.00
     * orderStatus : 0
     * settleDate : 20160503
     * settleTime : 205620
     * cardType : 03
     * merchantPara : para1
     */

    /**
     * 处理结果,SUC0000：请求处理成功（不能仅通过该字段判断订单成功）
     *
     * 其他：请求处理失败
     */
    private String rspCode;
    /**
     * 详细信息,请求处理失败时返回错误描述
     */
    private String rspMsg;
    /**
     *	响应时间,银行返回该数据的时间
     * 格式：yyyyMMddHHmmss
     */
    private String dateTime;
    /**
     *商户分行号，4位数字
     */
    private String branchNo;
    /**
     *商户号，6位数字
     */
    private String merchantNo;
    /**
     *商户订单日期，格式：yyyyMMdd
     */
    private String date;
    /**
     * 商户订单号
     */
    private String orderNo;
    /**
     * 银行的订单流水号
     */
    private String bankSerialNo;
    /**
     * 交易币种,固定为：“10”
     */
    private String currency;
    /**
     * 订单金额,格式：xxxx.xx
     */
    private String orderAmount;
    /**
     * 费用金额,格式：xxxx.xx
     */
    private String fee;
    /**
     * 银行受理日期,格式：yyyyMMdd
     */
    private String bankDate;
    /**
     *  银行受理时间,格式：HHmmss
     */
    private String bankTime;
    /**
     * 结算金额,格式：xxxx.xx
     */
    private String settleAmount;
    /**
     *  优惠金额,格式：xxxx.xx
     *  无优惠时返回0.00
     */
    private String discountAmount;
    /**
     * 订单状态,
     * 0:已结帐
     * 1:已撤销
     * 2:部分结账
     * 4:未结帐
     * 6:未知状态/订单失败
     * 7:冻结交易—冻结金额已经全部结账
     * 8:冻结交易，冻结金额只结账了一部分
     */
    private String orderStatus;
    /**
     * 银行处理日期,格式：yyyyMMdd
     */
    private String settleDate;
    /**
     * 银行处理时间,格式：HHmmss
     */
    private String settleTime;
    /**
     * 卡类型,02：本行借记卡；
     * 03：本行贷记卡；
     * 08：他行借记卡；
     * 09：他行贷记卡
     */
    private String cardType;
    /**
     * 商户自定义参数,商户在支付接口中传送的merchantPara参数，超过100字节自动截断。
     */
    private String merchantPara;
}
