package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@Getter  @Setter
public class CmbQuerySettledOrderByBankDateRespDTO {


    /**
     * rspCode : SUC0000
     * rspMsg :
     * dateTime : 20160825101406
     * hasNext : N
     * nextKeyValue : HH00012016080611290816280608000000000010
     * dataCount : 2
     * dataList : branchNo,`merchantNo,`date,`orderNo,`bankSerialNo,`orderRefNo,`currency,`orderAmount,`fee,`acceptDate,`acceptTime,`settleAmount,`discountAmount,`orderStatus,`settleDate,`settleTime,`cardType
     0755,`000054,`20160806,`9999080601,`16280606400000000010,`,`10,`1000.00,`100.00,`20160806,`153313,`1000.00,`0.00,`0,`20160806,`153313,`02
     0755,`000054,`20160806,`9999136801,`16280608000000000010,`,`10,`0.19,`0.01,`20160806,`113535,`0.19,`0.00,`0,`20160806,`113535,`03,`para1
     */

    /**
     *处理结果,SUC0000：请求处理成功（不能仅通过该字段判断订单成功）
     * 其他:请求处理失败
     */
    private String rspCode;

    /**
     *详细信息,请求处理失败时返回失败原因
     */
    private String rspMsg;

    /**
     * 响应时间,银行返回该数据的时间。 格式：yyyyMMddHHmmss
     */
    private String dateTime;

    /**
     * 续传标志,Y：有续传（还有更多记录）；N：无续传（记录已返回完毕）。
     */
    private String hasNext;

    /**
     * 续传键值,当hasNext=Y时必填
     */
    private String nextKeyValue;

    /**
     * 返回条数,本次查询返回条数。
     */
    private String dataCount;

    /**
     * 记录数据,每笔记录一行，行之间以\r\n分隔。 第一行为表头； 从第二行起为数据记录； 行内的参数以逗号和`符号分隔(`为标准键盘1 左边键的字符)，字段顺序与表头一致。 数据记录定义如下：
     *
     * branchNo	String(4)	M	商户分行号，4位数字	0755
     * merchantNo	String(6)	M	商户号，6位数字	000054
     * date	String(8)	M	商户的订单日期，格式：yyyyMMdd	20160624
     * orderNo	String(32)	M	商户的订单号	9999000001
     * bankSerialNo	String(20)	M	银行的订单流水号	20160624
     * orderRefNo	String(32)	O	订单参考号
     * currency	String(2)	M	交易币种,固定为：“10”	10
     * orderAmount	String	M	订单金额格式：xxxx.xx	0.01
     * fee	String	M	费用金额,格式：xxxx.xx	0.01
     * acceptDate	String(8)	M	银行受理日期,格式：yyyyMMdd	20160624
     * acceptTime	String(6)	M	银行受理时间,格式：HHmmss	121201
     * settleAmount	String	M	结算金额,格式：xxxx.xx	0.01
     * discountAmount	String(14)	M	优惠金额,单位为元，精确到小数点后两位； 格式为：xxxx.xx元；无优惠则返回0.00	0.00
     * orderStatus	String(3)	M	订单状态,0：已结账
     * 1：已撤销
     * 2：部分结账
     * 4：未结账
     * 5：无效状态
     * 6：未知状态/订单失败	0代表终态
     * settleDate	String(8)	M	银行处理日期,格式：yyyyMMdd	20160624
     * settleTime	String(6)	M	银行处理时间,格式：HHmmss	121201
     * cardType	String	M	卡类型,02：一卡通
     * 03：信用卡
     * 08：他行储蓄卡
     * 09：他行信用卡	02
     * merchantPara	String(100)	O	商户自定义参数,商户在支付接口中传送的merchantPara参数，超过100字节自动截断。
     *
     */
    private String dataList;
    /**
     * 提示：
     *
     * 1）对于通过调用接口发起的订单，收到报文中rspCode=‘SUC0000’且orderStatus=‘0’，代表该笔订单处理成功且已经结账。
     */
}
