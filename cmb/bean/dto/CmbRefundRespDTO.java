package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;

/***
 * "rspData":{
 *         "rspCode":"SUC0000",
 *         "rspMsg":"",
 *         "dateTime":"20160806150217",
 *         "bankSerialNo":"16280672300000000010",
 *         "currency":"10",
 *         "amount":"0.01",
 *         "refundRefNo":"608061000002",
 *         "bankDate":"20160806",
 *         "bankTime":"150052"
 *         “refundSerialNo”:”123456789”
 *         “settleAmount”:”0.01”
 *         “discountAmount”:”0.01”
 *     }
 */
@Getter  @Setter
public class CmbRefundRespDTO {

    /**
     * 处理结果,SUC0000：请求处理成功
     * 其他：请求处理失败
     */
    private String rspCode ;
    /**
     * 详细信息,请求处理失败时返回错误描述
     */
    private String rspMsg ;
    /**
     * 响应时间,银行返回该数据的时间
     * 格式：yyyyMMddHHmmss
     */
    private String dateTime ;
    /**
     * 银行的退款流水号
     */
    private String bankSerialNo ;
    /**
     * 退款币种,固定为：“10”
     */
    private String currency ;
    /**
     * 退款金额,格式：xxxx.xx
     */
    private String amount ;
    /**
     *银行的退款参考号
     */
    private String refundRefNo ;
    /**
     * String(8)
     * 退款受理日期
     * 格式：yyyyMMdd
     */
    private String bankDate ;
    /**
     * 退款受理时间
     * 格式：HHmmss
     */
    private String bankTime ;
    /**
     * 商户上送流水号
     */
    private String refundSerialNo ;
    /**
     * 退款标识字段
     * 空/“A”：按照订单金额发起退款（适用于自动清算/手工清算优惠交易）
     */
    private String settleAmount ;
    /**
     * 退回优惠金额,格式：xxxx.xx
     */
    private String discountAmount ;



}
