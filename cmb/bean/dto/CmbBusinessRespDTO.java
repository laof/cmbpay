package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter  @Setter
public class CmbBusinessRespDTO {


    /**
     * rspCode : SUC0000
     * rspMsg : 查询成功。
     * dateTime : 20160623101430
     * merchantSerialNo : 2016062310143088
     * agrNo : 201606238888888
     * userSignDateTime : 20160623101432
     * userPidType : 1
     * userPidHash : 501010131010181016101010101012
     * noPwdPay : N
     */

    /**
     * 处理结果,SUC0000：请求处理成功
     * 其他：请求处理失败
     */
    private String rspCode;
    /**
     * 详细信息,处理结果返回码的描述信息
     */
    private String rspMsg;
    /**
     * 响应时间,格式：yyyyMMddHHmmss
     * 含义：银行返回该数据的时间
     */
    private String dateTime;
    /**
     * 原样返回商户做请求时的流水号
     */
    private String merchantSerialNo;
    /**
     * 客户协议号，成功交易返回
     */
    private String agrNo;
    /**
     * 客户签订协议的时间yyyyMMddHHmmss
     * 成功交易返回
     */
    private String userSignDateTime;
    /**
     * 证件类型 目前只有”1”，表示身份证
     */
    private String userPidType;
    /**
     * 证件号映射的30位hash值
     * 成功交易返回
     */
    private String userPidHash;
    /**
     * 免密标识，固定为”N”，表示不开通免密。
     */
    private String noPwdPay;
}
