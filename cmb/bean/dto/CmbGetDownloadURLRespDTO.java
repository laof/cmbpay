package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter  @Setter
public class CmbGetDownloadURLRespDTO {


    /**
     * rspCode : SUC0000
     * rspMsg :
     * dateTime : 20160825113210
     * downloadUrl : http://xyz.com/abc.txt
     * date : 20160116
     * branchNo : 0755
     * merchantNo : 000123
     */

    /**
     *  处理结果
     * SUC0000:请求处理成功
     * 其他：请求处理失败
     */
    private String rspCode;
    /**
     * 详细信息，请求处理失败时返回错误描述
     */
    private String rspMsg;
    /**
     * 响应时间。银行返回该数据的时间格式：yyyyMMddHHmmss
     */
    private String dateTime;
    /**
     * 查询成功时（rspCode=SUC0000）返回。文件的下载地址，
     * 必须在有效期内下载，过期则无法下载。 目前有效期是：获取链接时起的1小时内
     */
    private String downloadUrl;
    /**
     * 商户订单日期，格式：yyyyMMdd
     */
    private String date;
    /**
     * 商户分行号，4位数字
     */
    private String branchNo;
    /**
     * 商户号，6位数字
     */
    private String merchantNo;


    /**
     * 下载文件 - CSV文件字段信息
     *
     * 交易日期	S(8,0)	20180601
     * 商户号	A(10)	0755630001
     * 商户订单号	A(32)
     * 交易类型	A(20)	消费；退款
     * 交易金额	S(13,2)
     * 结账货币	O(10)	人民币；美元；港币
     * 结账金额	S(13,2)	举例：0.01
     * 费用金额	S(13,2)	举例：0.00
     * 优惠金额	S(13,2)	举例：0.00
     * 结算日期	S(8,0)	20180601
     * 卡类型	O(20)	招行借记卡；招行信用卡；他行借记卡；他行信用卡；
     * 支付方式	O(20)	一网通支付；微信支付；支付宝；银联；一网通免密支付；
     * 附加数据	A(200)
     */



}
