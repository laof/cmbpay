package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;

@Getter  @Setter
public class CmbPayReqDTO {


    /**
     * dateTime : 20161209112230
     * branchNo : 0755
     * merchantNo : 000054
     * date : 20161209
     * orderNo : 9999000001
     * amount : 0.01
     * expireTimeSpan : 30
     * payNoticeUrl : http://www.merchant.com/path/payNotice.do
     * payNoticePara : 12345678|ABCDEFG|HIJKLM
     * returnUrl : http://www.merchant.com/path/return.do
     * clientIP : 99.12.38.165
     * cardType : A
     * agrNo : 12345678901234567890
     * merchantSerialNo : 2016062014308888
     * userID : 2016062388888
     * mobile : 18202532653
     * lon : 30.949505
     * lat : 50.949506
     * riskLevel :
     * signNoticeUrl : http://www.merchant.com/path/signNotice.do
     * signNoticePara : 12345678|ABCDEFG|HIJKLM
     * extendInfo : C7788996853803BB6981F406BA84939CE96B8D57FFB20B653078B181FA9691D7CFF00D1D3EF21E6B338A3FD33690CAF12078BE885BB49F6016CF5A0314D21BF49738629F8386CB437A76A8DBE1E7E932DBDB18B7C69A8D10900FB8A3C98CB48833B5800A541DD6B5A12F65508C3BCD1D51B32DD02CCE362BB5CE4D95E5D29921E603BBE3CA5B650652589011304CC2969E2A1046F91A1849B70001FE23EA8592560BE4FB7994D51BC7E7F5F5E08474F7090A44D635F2BFD7353B081DF80613817FD357CD472392A2EFEB1DA52D72DADD3BD5725FE42FA9D4B19
     * extendInfoEncrypType : RC4
     */

    /**
     * 请求时间,格式：yyyyMMddHHmmss
     * 商户发起该请求的当前时间，精确到秒
     */
    private String dateTime;
    /**
     * 分行号，4位数字
     */
    private String branchNo;
    /***
     * 商户号，6位数字
     */
    private String merchantNo;
    /**
     * 订单日期,格式：yyyyMMdd
     */
    private String date;
    /**
     * 订单号。 支持6-32位（含6和32）间任意位数的订单号，支持不固定位数，支持数字+字母（大小字母）随意组合
     * 。由商户生成，一天内不能重复。 订单日期+订单号唯一定位一笔订单。
     */
    private String orderNo;
    /**
     * 订单金额，格式：xxxx.xx
     * 固定两位小数，最大11位整数
     */
    private String amount;
    /**
     * 过期时间跨度，必须为大于零的整数，单位为分钟。该参数指定当前支付请求必须在指定时间跨度内完成（从系统收到支付请求开始计时）
     * ，否则按过期处理。一般适用于航空客票等对交易完成时间敏感的支付请求。
     */
    private String expireTimeSpan;
    /**
     * 商户接收成功支付结果通知的地址。
     */
    private String payNoticeUrl;
    /**
     * 成功支付结果通知附加参数,该参数在发送成功支付结果通知时，将原样返回商户注意：该参数可为空，商户如果需要不止一个参数，可以自行把参数组合、拼装，但组合后的结果不能带有’&’字符。
     */
    private String payNoticePara;
    /**
     *返回商户地址，支付成功页面、支付失败页面上“返回商户”按钮跳转地址。如客户多次输入支付密码错误后提示“改日再试”按钮也会跳转该地址。
     * 为空则不显示返回商户按钮。
     * 原生APP可传入一个特定地址（例如:Http://CMBNPRM），并拦截处理自行决定跳转交互。
     */
    private String returnUrl;
    /**
     * 商户取得的客户IP，如果有多个IP用逗号”,”分隔。
     */
    private String clientIP;
    /**
     * 允许支付的卡类型，默认对支付卡种不做限制，储蓄卡和信用卡均可支付
     * A:储蓄卡支付，即禁止信用卡支付
     */
    private String cardType;
    /**
     * 客户协议号。如商户上送协议号，客户再次支付无需进行登录操作,推荐上送。
     * 支持数字、字母（大小字母）、“-””_”两个特殊字符（注意-_是英文半角的）。
     * 未签约（首次支付）客户，填写新协议号，用于协议开通；
     * 已签约（再次支付）客户，填写该客户已有的协议号。商户必须对协议号进行管理，确保客户与协议号一一对应。
     */
    private String agrNo;
    /**
     * 协议开通请求流水号，开通协议时必填。
     */
    private String merchantSerialNo;
    /**
     * 商户用户ID,用于标识商户用户的唯一ID。商户系统内用户唯一标识，不超过20位，数字字母都可以，建议纯数字
     */
    private String userID;
    /**
     * 商户用户的手机号
     */
    private String mobile;
    /**
     * 经度，商户app获取的手机定位数据，如 30.949505
     */
    private String lon;
    /**
     * 纬度，商户app获取的手机定位数据，如 50.949506
     */
    private String lat;
    /**
     * 风险等级,用户在商户系统内风险等级标识
     */
    private String riskLevel;
    /**
     * 成功签约结果通知地址，上送协议号且首次签约，必填。商户接收成功签约结果通知的地址。
     */
    private String signNoticeUrl;
    /**
     * 成功签约结果通知附加参数,该参数在发送成功签约结果通知时，将原样返回商户
     * 注意：该参数可为空，商户如果需要不止一个参数，可以自行把参数组合、拼装，但组合后的结果不能带有’&’字符。
     */
    private String signNoticePara;
    /**
     * 扩展信息，json格式写入的扩展信息，并使用extendInfoEncrypType指定的算法加密使用详见扩展信息注意：1.加密后的密文必须转换为16进制字符串2.如果扩展信息字段非空，该字段必填
     */
    private String extendInfo;
    /**
     * 扩展信息的加密算法,扩展信息加密类型，取值为RC4或DES
     * 加密密钥：
     * 取值为RC4时，密钥为商户支付密钥；
     * 取值为DES时，密钥为商户支付密钥的前8位，不足8位则右补0
     * 注意：如果扩展信息字段非空，该字段必填
     */
    private String extendInfoEncrypType;


}
