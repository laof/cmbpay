package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;
import lombok.NoArgsConstructor;

/***
 */
@NoArgsConstructor
@Getter  @Setter
public class CmbSettledRefundQueryReqDTO {


    /**
     "dateTime":"20161227161057",
     "branchNo":"0755",
     "merchantNo":"000054",
     "date":"20161105",
     "orderNo":"1336480523",
     "type":"C",
     "merchantSerialNo":"",
     "bankSerialNo":""
     */

    /**
     * 请求时间,商户发起该请求的当前时间，精确到秒。 格式：yyyyMMddHHmmss
     */
    private String dateTime;
    /**
     * 商户分行号，4位数字
     */
    private String branchNo;
    /**
     * 商户号，6位数字
     */
    private String merchantNo;
    /**
     * 查询类型
     * A：按银行退款流水号查单笔
     * B：按商户订单号+商户退款流水号查单笔
     * C: 按商户订单号查退款
     */
    private String type;
    /**
     * 商户订单日期，格式：yyyyMMdd
     */
    private String date	;
    /**
     * 商户订单号
     */
    private String orderNo	;
    /**
     * 商户退款流水号长度不超过20位
     * 1.商户送商户退款流水号查单笔；
     * 2.商户不送商户退款流水号，查退款订单；
     */
    private String merchantSerialNo	;
    /**
     * 银行退款流水号长度不超过20位
     */
    private String bankSerialNo;
}
