package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;
import lombok.NoArgsConstructor;

/***
 */
@NoArgsConstructor
@Getter  @Setter
public class CmbSettledRefundQueryRespDTO {

    /**
     *
     */
    private String rspCode;
    private String rspMsg;
    private String dateTime;
    private String dataCount;

    /**
     *
     *每笔记录一行，每行以\r\n 结束。第一行为表头;从第二行起为数据记录;各参数以逗号和`符号分隔(`为标准键盘1 左边键的字符)，
     * 字段顺序与表头一致。数据记录定义如下：
     *
     * 分行号	branchNo	String(4)	M	商户分行号，4位数字	0755
     * 商户号	merchantNo	String(6)	M	商户号，6位数字	000054
     * 订单日期	date	String(8)	M	商户的订单日期，格式：yyyyMMdd	20160624
     * 订单号	orderNo	String(32)	M	商户的订单号	9999000001
     * 银行退款流水号	bankSerialNo	String(20)	M	银行退款流水号	16280587500000000020
     * 商户退款流水号	merchantSerialNo	String(20)	O	商户上送流水号	20161101180000010001
     * 退款状态	orderStatus	String(3)	M
     * 210：已直接退款
     * 219：直接退款已受理
     * 240：已授权退款
     * 249：授权退款已受理
     * 999：退款失败
     * 币种	currency	String(2)	M	固定为：“10”	10
     * 退款金额	amount	String	M	格式：xxxx.xx	0.01
     * 费用金额	fee	String	M	格式：xxxx.xx	0.01
     * 银行受理日期	acceptDate	String(8)	M	格式：yyyyMMdd	20160624
     * 银行受理时间	acceptTime	String(6)	M	格式：HHmmss	121201
     * 银行处理日期	bankDate	String(8)	M	格式：yyyyMMdd	20160624
     * 银行处理时间	bankTime	String(6)	M	格式：HHmmss	121201
     * 退款描述	desc	String(100)	O	退款描述	ABC123
     * 实际退款金额	settleAmount	String	M	格式：xxxx.xx	0.01
     * 退回优惠金额	discountAmount	String	M	格式：xxxx.xx	0.01
     */
    private String dataList;
}
