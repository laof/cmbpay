package com.forhome.zhijia.utils.cmb.bean.dto;

import lombok.Getter; import lombok.Setter;
import lombok.NoArgsConstructor;

/***
 */
@NoArgsConstructor
@Getter  @Setter
public class CmbQueryAccountListRespDTO {


    /**
     * rspCode : SUC0000
     * rspMsg :
     * dateTime : 20160825144658
     * hasNext : N
     * nextKeyValue : HH00012016080508381016280587500000000010
     * dataCount : 2
     * dataList : branchNo,`merchantNo,`date,`orderNo,`bankSerialNo,`orderRefNo,`refundSerialNo,`orderStatus,`currency,`amount,`fee,`refundDesc,`bankDate,`bankTime
     0755,`000054,`20160804,`9999000003,`16280481900000000010,`,`16280535600000000010,`210,`10,`30.00,`3.00,`,`20160805,`083849
     0755,`000054,`20160804,`9999000005,`16280418800000000010,`,`16280587500000000010,`210,`10,`61.00,`6.10,`,`20160805,`083810
     */

    /**
     * 处理结果,SUC0000：请求处理成功（不能仅通过该字段判断退款成功）
     *
     * 其他：请求处理失败
     */
    private String rspCode;
    /**
     * 详细信息,请求处理失败时返回错误描述
     */
    private String rspMsg;
    /**
     * 响应时间,银行返回该数据的时间
     * 格式：yyyyMMddHHmmss
     */
    private String dateTime;
    /**
     * 续传标志,Y:有续传（还有更多记录）； N：无续传（记录已返回完毕）。
     */
    private String hasNext;
    /**
     * 续传键值,当hasNext=Y时必传。
     */
    private String nextKeyValue;
    /**
     * 本次查询返回条数
     */
    private String dataCount;

    /**
     *
     * 记录数据,每笔记录一行，行之间以\r\n分隔。 第一行为表头; 从第二行起为数据记录; 行内的参数以逗号和`符号分隔(`为标准键盘1 左边键的字符)，字段顺序与表头一致。 数据记录定义如下：
     *
     * branchNo	String(4)	M	商户分行号，4位数字	0755
     * merchantNo	String(6)	M	商户号，6位数字	000054
     * date	String(8)	M	商户订单日期，格式：yyyyMMdd	20160624
     * orderNo	String(32)	M	商户订单号	9999000001
     * bankSerialNo	String(20)	M	银行的订单流水号	16250327200000000020
     * orderRefNo	String(32)	O	订单参考号
     * refundSerialNo	String(20)	M	银行退款流水号	16280535600000000010
     * orderStatus	String(3)	M	210：已直接退款
     * 219：直接退款已受理
     * 240：已授权退款
     * 249：授权退款已受理
     * 999：退款失败	210
     * currency	String(2)	M	币种,固定为：“10”	10
     * amount	String	M	退款金额,格式：xxxx.xx	0.01
     * fee	String	M	费用金额,格式：xxxx.xx	0.01
     * refundDesc	String	O	退款说明
     * bankDate	String(8)	M	银行受理日期,格式：yyyyMMdd	20160624
     * bankTime	String(6)	M	银行受理时间,格式：HHmmss	121201
     * settleAmount	String	M	实际退款金额,格式：xxxx.xx	0.01
     * discountAmount	String	M	退回优惠金额,格式：xxxx.xx	0.01
     *
     */
    private String dataList;

}
