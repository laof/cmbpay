package com.forhome.zhijia.utils.cmb.bean;

import lombok.Getter; import lombok.Setter;


/**
 * @param <T>
 */
@Getter  @Setter
public class NoticeReqBody<T> {

    private String version = "1.0";

    private String charset = "UTF-8" ;

    private String signType = "RSA";

    /**
     * 报文签名,使用商户支付密钥对reqData内的数据进行签名
     */
    private String sign ;

    /**
     * 请求数据
     */
    private T noticeData ;







}
