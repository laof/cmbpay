package com.forhome.zhijia.utils.cmb.bean;

import lombok.Getter; import lombok.Setter;

@Getter  @Setter
public class RespBody<T> {

    /**
     * 接口版本号,固定为”1.0”
     */
    private String version = "1.0";

    /**
     * 参数编码,固定为UTF-8
     */
    private String charset = "UTF-8" ;

    /**
     * 签名算法,固定为”SHA-256”
     */
    private String signType = "SHA-256";

    /**
     * 报文签名,使用商户支付密钥对rspData内的数据进行验签
     */
    private String sign ;

    /**
     * 请求数据
     */
    private T rspData ;







}
