package com.forhome.zhijia.utils.cmb.bean;

import lombok.Getter; import lombok.Setter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter  @Setter
public class ReqBody<T> {

    private String version = "1.0";

    private String charset = "UTF-8" ;

    private String signType = "SHA-256";

    /**
     * 报文签名,使用商户支付密钥对reqData内的数据进行签名
     */
    private String sign ;

    /**
     * 请求数据
     */
    private T reqData ;


    public ReqBody(T reqData) {
        this.reqData = reqData;
    }
}
